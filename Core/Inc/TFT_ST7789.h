/*
 * TFT_ST7789.h
 *
 *  Created on: 27 wrz 2022
 *      Author: Mateusz Salamon
 */

#ifndef INC_TFT_ST7789_H_
#define INC_TFT_ST7789_H_

#define ST7789_ROTATION 0 // 0 - 0*, 1 - 90*, 2 - 180*, 3 - 270*

#define ST7789_USE_HW_RESET 1
#define ST7789_USE_CS 1

#define ST7789_OPTIMIZE_HAL_SP1 1

//
// Pin operation
//
#define ST7789_CS_LOW HAL_GPIO_WritePin(TFT_CS_GPIO_Port, TFT_CS_Pin, GPIO_PIN_RESET)
#define ST7789_CS_HIGH HAL_GPIO_WritePin(TFT_CS_GPIO_Port, TFT_CS_Pin, GPIO_PIN_SET)

#define ST7789_DC_LOW HAL_GPIO_WritePin(TFT_DC_GPIO_Port, TFT_DC_Pin, GPIO_PIN_RESET)
#define ST7789_DC_HIGH HAL_GPIO_WritePin(TFT_DC_GPIO_Port, TFT_DC_Pin, GPIO_PIN_SET)

#define ST7789_RST_LOW HAL_GPIO_WritePin(TFT_RST_GPIO_Port, TFT_RST_Pin, GPIO_PIN_RESET)
#define ST7789_RST_HIGH HAL_GPIO_WritePin(TFT_RST_GPIO_Port, TFT_RST_Pin, GPIO_PIN_SET)

#define ST7789_SPI_TIMEOUT 1000

//
// Registers
//

#if (ST7789_ROTATION == 0) || (ST7789_ROTATION == 2)
#define ST7789_TFTWIDTH 240  ///< ST7789 max TFT width
#define ST7789_TFTHEIGHT 320 ///< ST7789 max TFT height
#elif (ST7789_ROTATION == 1) || (ST7789_ROTATION == 3)
#define ST7789_TFTWIDTH 320  ///< ST7789 max TFT width
#define ST7789_TFTHEIGHT 240 ///< ST7789 max TFT height
#endif

#define ST_CMD_DELAY 0x80 // special signifier for command lists

#define ST77XX_NOP 0x00
#define ST77XX_SWRESET 0x01
#define ST77XX_RDDID 0x04
#define ST77XX_RDDST 0x09

#define ST77XX_SLPIN 0x10
#define ST77XX_SLPOUT 0x11
#define ST77XX_PTLON 0x12
#define ST77XX_NORON 0x13

#define ST77XX_INVOFF 0x20
#define ST77XX_INVON 0x21
#define ST77XX_DISPOFF 0x28
#define ST77XX_DISPON 0x29
#define ST77XX_CASET 0x2A
#define ST77XX_RASET 0x2B
#define ST77XX_RAMWR 0x2C
#define ST77XX_RAMRD 0x2E

#define ST77XX_PTLAR 0x30
#define ST77XX_TEOFF 0x34
#define ST77XX_TEON 0x35
#define ST77XX_MADCTL 0x36
#define ST77XX_COLMOD 0x3A

#define ST77XX_MADCTL_MY 0x80
#define ST77XX_MADCTL_MX 0x40
#define ST77XX_MADCTL_MV 0x20
#define ST77XX_MADCTL_ML 0x10
#define ST77XX_MADCTL_RGB 0x00

#define ST77XX_RDID1 0xDA
#define ST77XX_RDID2 0xDB
#define ST77XX_RDID3 0xDC
#define ST77XX_RDID4 0xDD

// Some ready-made 16-bit ('565') color settings:
#define ST77XX_BLACK 0x0000
#define ST77XX_WHITE 0xFFFF
#define ST77XX_RED 0xF800
#define ST77XX_GREEN 0x07E0
#define ST77XX_BLUE 0x001F
#define ST77XX_CYAN 0x07FF
#define ST77XX_MAGENTA 0xF81F
#define ST77XX_YELLOW 0xFFE0
#define ST77XX_ORANGE 0xFC00

void ST7789_Init(SPI_HandleTypeDef *hspi);
void ST7789_SetRotation(uint8_t Rotation);
void ST7789_WritePixel(int16_t x, int16_t y, uint16_t color);
void ST7789_ClearDisplay(uint16_t Color);
void ST7789_DrawImage(uint16_t x, uint16_t y, const uint8_t *img, uint16_t w, uint16_t h);

#endif /* INC_TFT_ST7789_H_ */
