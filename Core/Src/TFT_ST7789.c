/*
 * TFT_ST7789.c
 *
 *  Created on: 27 wrz 2022
 *      Author: Mateusz Salamon
 */
#include "main.h"
#include "TFT_ST7789.h"

static SPI_HandleTypeDef *Tft_hspi; // SPI Handler

//
//  Basic function - write those for your MCU
//
static void ST7789_Delay(uint32_t ms)
{
	HAL_Delay(ms);
}

static void ST7789_SendToTFT(uint8_t *Byte, uint32_t Length)
{
#if (ST7789_OPTIMIZE_HAL_SP1 == 1)
	// That is taken from HAL Transmit function
    while (Length > 0U)
    {
      /* Wait until TXE flag is set to send data */
      if(__HAL_SPI_GET_FLAG(Tft_hspi, SPI_FLAG_TXE))
      {
    	//Fill Data Register in SPI
        *((__IO uint8_t *)&Tft_hspi->Instance->DR) = (*Byte);
        // Next byte
        Byte++;
        // Length decrement
        Length--;
      }
    }

    // Wait for Transfer end
	while(__HAL_SPI_GET_FLAG(Tft_hspi, SPI_FLAG_BSY) != RESET)
	{

	}
#else
	HAL_SPI_Transmit(Tft_hspi, Byte, Length, ST7789_SPI_TIMEOUT); 	// Send the command byte
#endif
}

static void ST7789_SendCommand(uint8_t Command)
{
	// CS Low
#if (ST7789_USE_CS == 1)
	ST7789_CS_LOW;
#endif
	// DC to Command - DC to Low
	ST7789_DC_LOW;

	// Send to TFT 1 byte
	ST7789_SendToTFT(&Command, 1);

	// CS High
#if (ST7789_USE_CS == 1)
	ST7789_CS_HIGH;
#endif
}

static void ST7789_SendCommandAndData(uint8_t Command, uint8_t *Data, uint32_t Length)
{
	// CS Low
#if (ST7789_USE_CS == 1)
	ST7789_CS_LOW;
#endif
	// DC to Command - DC to Low
	ST7789_DC_LOW;
	// Send to TFT 1 byte
	ST7789_SendToTFT(&Command, 1);

	// DC to Data - DC to High
	ST7789_DC_HIGH;
	// Send to TFT Length byte
	ST7789_SendToTFT(Data, Length);

	// CS High
#if (ST7789_USE_CS == 1)
	ST7789_CS_HIGH;
#endif
}
#if (ST7789_OPTIMIZE_HAL_SP1 == 0)
static void ST7789_SendData16(uint16_t Data)
{
#if (ST7789_USE_CS == 1)
	ST7789_CS_LOW;
#endif

	uint8_t tmp[2];
	tmp[0] = (Data>>8);
	tmp[1] = Data & 0xFF;

	ST7789_DC_HIGH;	// Data mode
	ST7789_SendToTFT(tmp, 2);

#if (ST7789_USE_CS == 1)
	ST7789_CS_HIGH;
#endif
}
#endif

//
// TFT Functions
//
void ST7789_SetRotation(uint8_t Rotation)
{
	if(Rotation > 3)
		return;

	//
	// Set appropriate bits for Rotation
	//
	switch(Rotation)
	{
	case 0:
		Rotation = (ST77XX_MADCTL_MX | ST77XX_MADCTL_MY | ST77XX_MADCTL_RGB);
		break;
	case 1:
		Rotation = (ST77XX_MADCTL_MY | ST77XX_MADCTL_MV | ST77XX_MADCTL_RGB);
		break;
	case 2:
		Rotation = (ST77XX_MADCTL_RGB);
		break;
	case 3:
		Rotation = (ST77XX_MADCTL_MX | ST77XX_MADCTL_MV | ST77XX_MADCTL_RGB);
		break;
	}

	// Write indo MAD Control register our Rotation data
	ST7789_SendCommandAndData(ST77XX_MADCTL, &Rotation, 1);
}

void ST7789_SetAddrWindow(uint16_t x1, uint16_t y1, uint16_t w, uint16_t h)
{
	uint8_t DataToTransfer[4];
	// Calculate end ranges
	uint16_t x2 = (x1 + w - 1), y2 = (y1 + h - 1);

	// Fulfill X's buffer
	DataToTransfer[0] = x1 >> 8;
	DataToTransfer[1] = x1 & 0xFF;
	DataToTransfer[2] = x2 >> 8;
	DataToTransfer[3] = x2 & 0xFF;
	// Push X's buffer
	ST7789_SendCommandAndData(ST77XX_CASET, DataToTransfer, 4);

	// Fulfill Y's buffer
	DataToTransfer[0] = y1 >> 8;
	DataToTransfer[1] = y1 & 0xFF;
	DataToTransfer[2] = y2 >> 8;
	DataToTransfer[3] = y2 & 0xFF;
	// Push Y's buffer
	ST7789_SendCommandAndData(ST77XX_RASET, DataToTransfer, 4);
}

void ST7789_WritePixel(int16_t x, int16_t y, uint16_t color)
{
	uint8_t DataToTransfer[2];

	if ((x >= 0) && (x < ST7789_TFTWIDTH) && (y >= 0) && (y < ST7789_TFTHEIGHT))
	{
		// Set Window for 1x1 pixel
		ST7789_SetAddrWindow(x, y, 1, 1);

		// Fulfill buffer with color
		DataToTransfer[0] = color >> 8;
		DataToTransfer[1] = color & 0xFF;
		// Push color bytes to RAM
		ST7789_SendCommandAndData(ST77XX_RAMWR, DataToTransfer, 2);
	}
}

void ST7789_DrawImage(uint16_t x, uint16_t y, const uint8_t *img, uint16_t w, uint16_t h)
{
	// Check if image will fit into screen - cannot make it outside by hardware
	if ((x >= 0) && ((x + w) <= ST7789_TFTWIDTH) && (y >= 0) && ((y + h) <= ST7789_TFTHEIGHT))
	{
		// Set window for image
		ST7789_SetAddrWindow(x, y, w, h);
		// Push image to RAM
		ST7789_SendCommandAndData(ST77XX_RAMWR, (uint8_t *)img, (w*h*2));
	}
}

void ST7789_ClearDisplay(uint16_t Color)
{
	// Set window for whole screen
	ST7789_SetAddrWindow(0, 0, ST7789_TFTWIDTH, ST7789_TFTHEIGHT);
	// Set RAM writing
	ST7789_SendCommand(ST77XX_RAMWR);

#if (ST7789_OPTIMIZE_HAL_SP1 == 1)
	uint32_t Length = ST7789_TFTWIDTH * ST7789_TFTHEIGHT;

#if (ST7789_USE_CS == 1)
	ST7789_CS_LOW;
#endif
	ST7789_DC_HIGH;	// Data mode

    while (Length > 0U)
    {
      /* Wait until TXE flag is set to send data */
      if(__HAL_SPI_GET_FLAG(Tft_hspi, SPI_FLAG_TXE))
      {
    	  // Write higher byte of color to DR
        *((__IO uint8_t *)&Tft_hspi->Instance->DR) = (Color >> 8);
        // Wait for transfer
        while(__HAL_SPI_GET_FLAG(Tft_hspi, SPI_FLAG_TXE) != SET)
        {}
        // Write lower byt of color to DR
        *((__IO uint8_t *)&Tft_hspi->Instance->DR) = (Color & 0xFF);
        // Decrease Lenght
        Length--;
      }
    }

    // Wait for the end of transfer
	while(__HAL_SPI_GET_FLAG(Tft_hspi, SPI_FLAG_BSY) != RESET)
	{

	}

#if (ST7789_USE_CS == 1)
	ST7789_CS_HIGH;
#endif
#else
	for(uint32_t i = 0; i < (ST7789_TFTWIDTH * ST7789_TFTHEIGHT); i++)
	{
		// Send Color bytes
		ST7789_SendData16(Color);
	}
#endif
}


static const uint8_t initcmd[] = {
	9,                              //  9 commands in list:
	ST77XX_SWRESET,   ST_CMD_DELAY, //  1: Software reset, no args, w/delay
	  150,                          //     ~150 ms delay
	ST77XX_SLPOUT ,   ST_CMD_DELAY, //  2: Out of sleep mode, no args, w/delay
	  10,                          //      10 ms delay
	ST77XX_COLMOD , 1+ST_CMD_DELAY, //  3: Set color mode, 1 arg + delay:
	  0x55,                         //     16-bit color
	  10,                           //     10 ms delay
	ST77XX_MADCTL , 1,              //  4: Mem access ctrl (directions), 1 arg:
	  0x08,                         //     Row/col addr, bottom-top refresh
	ST77XX_CASET  , 4,              //  5: Column addr set, 4 args, no delay:
	  0x00,
	  0,        //     XSTART = 0
	  0,
	  240,  //     XEND = 240
	ST77XX_RASET  , 4,              //  6: Row addr set, 4 args, no delay:
	  0x00,
	  0,             //     YSTART = 0
	  320>>8,
	  320&0xFF,  //     YEND = 320
	ST77XX_INVOFF  ,   ST_CMD_DELAY,  //  7: hack
	  10,
	ST77XX_NORON  ,   ST_CMD_DELAY, //  8: Normal display on, no args, w/delay
	  10,                           //     10 ms delay
	ST77XX_DISPON ,   ST_CMD_DELAY, //  9: Main screen turn on, no args, delay
	  10                          //    10 ms delay                                  // End of list
};

void ST7789_Init(SPI_HandleTypeDef *hspi)
{
	Tft_hspi = hspi;

    uint8_t numCommands, cmd, numArgs;
    uint16_t ms;
    const uint8_t *addr = initcmd;

#if (ST7789_OPTIMIZE_HAL_SP1 == 1)
    __HAL_SPI_ENABLE(Tft_hspi);
#endif

#if (ST7789_USE_HW_RESET == 1)
	ST7789_RST_LOW;
	ST7789_Delay(10);
	ST7789_RST_HIGH;
	ST7789_Delay(10);
#else
	ST7789_SendCommand(ST7789_SWRESET); // Engage software reset
    ST7789_Delay(150);
#endif

    numCommands = *(addr++); // Number of commands to follow

    while (numCommands--) {              // For each command...
      cmd = *(addr++);       // Read command
      numArgs = *(addr++);   // Number of args to follow
      ms = numArgs & ST_CMD_DELAY;       // If hibit set, delay follows args
      numArgs &= ~ST_CMD_DELAY;          // Mask out delay bit
      ST7789_SendCommandAndData(cmd, (uint8_t *)addr, numArgs);
      addr += numArgs;

      if (ms) {
        ms = *(addr++); // Read post-command delay time (ms)
        if (ms == 255)
          ms = 500; // If 255, delay for 500 ms
        ST7789_Delay(ms);
      }
    }

    // Set selected Rotation
    ST7789_SetRotation(ST7789_ROTATION);
}
